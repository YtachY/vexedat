import React from 'react';
import FirstPage from './components/first-page'
import './App.scss';

const App = () => {
  return (
    <div>
      <FirstPage />
    </div>
  )
}

export default App
