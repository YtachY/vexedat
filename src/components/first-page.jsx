import React from "react";
import styles from "./FirstPage.module.scss";

const FirstPage = () => {
  return (
    <div className={styles.index}>
      <div className={styles.navbar}>
        <div className={styles.rectangle3} />
        <div className={styles.rectangle30} />
        <div className={styles.logo}>
          <div className={styles.jamcar}>
            <img
              alt=""
              src="https://static.overlay-tech.com/assets/9f1cef4e-7a7a-407d-ae51-6b6d94257c77.svg"
            />
          </div>
          <div className={styles.group}>
            <p className={styles.veXeDat}>VeXeDat</p>
          </div>
        </div>
        <div className={styles.buttonDangNhap}>
          <div className={styles.group2}>
            <img
              alt=""
              className={styles.groupTwo}
              src="https://static.overlay-tech.com/assets/fe904c3d-f1a9-4262-af8c-899e4147cac5.svg"
            />
            <p className={styles.dangNhap}>Dang nhap</p>
          </div>
        </div>
        <div className={styles.buttonHotline}>
          <div className={styles.akarIconsphone}>
            <img
              alt=""
              src="https://static.overlay-tech.com/assets/f8b6e040-f5e4-46af-9497-24b133d142f4.svg"
            />
          </div>
          <p className={styles.hotline}>Hotline</p>
        </div>
        <div className={styles.language}>
          <div className={styles.rienglishInput}>
            <img
              alt=""
              src="https://static.overlay-tech.com/assets/da7ccef4-ffd9-4d82-bc2b-ebe534e607a2.svg"
            />
          </div>
          <div className={styles.grommetIconslanguage}>
            <img
              alt=""
              src="https://static.overlay-tech.com/assets/3da293c8-62d8-4f93-8239-db64d9cce0f2.svg"
            />
          </div>
        </div>
        <p className={styles.uuDai304}>Uu dai 30/4</p>
        <p className={styles.xeLimouse}>Xe Limouse</p>
        <p className={styles.quanLyChuyenDi}>
          Quan ly chuyen di
        </p>
      </div>
      <div className={styles.landSearch}>
        <p className={styles.datSomGiaHoiDealHot304GiamDen}>
          Dat som gia hoi
          <br />
          deal hot 30/4, giam den 140k
        </p>
        <div className={styles.flexWrapperEight}>
          <p
            className={
              styles.veXeDatMlTuotVeTrongTamTayViGi
            }
          >
            VeXeDat.ml - Tuot ve trong tam tay vi gia qua
            cao
          </p>
          <div className={styles.group1}>
            <div className={styles.relativeWrapperThree}>
              <div className={styles.bxbxTransferAlt}>
                <img
                  alt=""
                  src="https://static.overlay-tech.com/assets/8d84aea7-debd-4611-bf17-1b8886b0c1e4.svg"
                />
              </div>
              <div className={styles.noiDi}>
                <div className={styles.psfacebookPlaces}>
                  <img
                    alt=""
                    src="https://static.overlay-tech.com/assets/9b80a52d-4dc3-4cf1-8f55-325475b565c0.svg"
                  />
                </div>
                <p className={styles.nhaTrang}>Nha Trang</p>
              </div>
              <div className={styles.flexWrapperSix}>
                <div className={styles.noiDen}>
                  <div
                    className={styles.psfacebookPlacesTwo}
                  >
                    <img
                      alt=""
                      src="https://static.overlay-tech.com/assets/ba9f00af-bd50-4b24-ad99-91969481f7a3.svg"
                    />
                  </div>
                  <p className={styles.nhaTrang}>Sai Gon</p>
                </div>
                <div className={styles.line1} />
              </div>
            </div>
            <div className={styles.flexWrapperSeven}>
              <div className={styles.line2} />
              <div className={styles.lich}>
                <div className={styles.icbaselineDateRange}>
                  <img
                    alt=""
                    src="https://static.overlay-tech.com/assets/8a640295-eaae-4a62-9fcf-d36f450c68d3.svg"
                  />
                </div>
                <p className={styles.num02052021}>
                  02 - 05 - 2021
                </p>
              </div>
            </div>
            <div className={styles.buttonDatChuyen}>
              <p className={styles.timChuyen}>TIM CHUYEN</p>
            </div>
          </div>
          <div className={styles.group2Two}>
            <div className={styles.group4}>
              <p className={styles.banDatXeDe}>
                Ban dat xe de
              </p>
              <div className={styles.flexWrapperFour}>
                <p className={styles.duLich}>Du lich</p>
                <div className={styles.flexWrapperFive}>
                  <p className={styles.veQue}>Ve que </p>
                </div>
                <p className={styles.duLich}>Cong tac</p>
              </div>
            </div>
            <div className={styles.group5}>
              <p className={styles.send}>Send</p>
            </div>
          </div>
        </div>
      </div>
      <div className={styles.body}>
        <p className={styles.uuDaiNoiBat}>Uu dai noi bat</p>
        <div className={styles.group3}>
          <p className={styles.benXeKhach}>Ben xe khach</p>
          <div className={styles.flexWrapperTen}>
            <img
              alt=""
              className={styles.rectangle34}
              src="https://static.overlay-tech.com/assets/522c5e0f-79c7-42ca-89e2-16abb100d22a.png"
            />
            <img
              alt=""
              className={styles.rectangle34}
              src="https://static.overlay-tech.com/assets/850e28b5-f1f1-43a4-b0ab-5b2312d63046.png"
            />
            <img
              alt=""
              className={styles.rectangle34}
              src="https://static.overlay-tech.com/assets/9f8f2614-82a0-4b0f-8d9e-3b3544dec514.png"
            />
            <img
              alt=""
              className={styles.rectangle34}
              src="https://static.overlay-tech.com/assets/a7565bcc-4f59-4728-a3b3-3041d920c456.png"
            />
          </div>
        </div>
        <div className={styles.flexWrapperThree}>
          <div className={styles.group1Two}>
            <img
              alt=""
              className={styles.rectangle28}
              src="https://static.overlay-tech.com/assets/1c10e2fa-3f9a-4b15-ac04-fac2e9352099.png"
            />
            <img
              alt=""
              className={styles.rectangle27}
              src="https://static.overlay-tech.com/assets/4a1efb18-859b-445a-a8d8-a57b590fa58d.png"
            />
            <img
              alt=""
              className={styles.rectangle28}
              src="https://static.overlay-tech.com/assets/01a7cb10-063b-4a6e-a39d-a127a4dcf7c3.png"
            />
          </div>
          <div className={styles.group2Three}>
            <p className={styles.benXeKhach}>
              Nen tang ket noi nguoi dung va nha xe
            </p>
            <div className={styles.flexWrapperTen}>
              <div className={styles.group6}>
                <img
                  alt=""
                  className={styles.groupThree}
                  src="https://static.overlay-tech.com/assets/53715413-61c5-4afe-953a-103ea92bd009.svg"
                />
                <div className={styles.flexWrapperTwelve}>
                  <p className={styles.caNhanHoaTimKiem}>
                    Ca nhan hoa tim kiem
                  </p>
                  <p className={styles.noiDung1}>
                    5000&#43; Tuyen duong, 2000&#43; nha xe,
                    5000&#43; dai ly tren khap ca nuoc. Chon
                    nha xe yeu thich cuc nhanh
                  </p>
                </div>
              </div>
              <div className={styles.group7}>
                <div
                  className={
                    styles.claritycursorHandClickLine
                  }
                >
                  <img
                    alt=""
                    src="https://static.overlay-tech.com/assets/3f957321-a33b-4142-91cf-f6f9dc5db7d3.svg"
                  />
                </div>
                <div className={styles.flexWrapperTwelve}>
                  <p className={styles.caNhanHoaTimKiem}>
                    Dat ve de dang
                  </p>
                  <p className={styles.noiDung1}>
                    Dat ve chi voi 60s. De dang lua chon
                    hinh thuc phu hop voi nhieu kenh thanh
                    toan da dang.
                  </p>
                </div>
              </div>
              <div className={styles.group8}>
                <div className={styles.entypoticket}>
                  <img
                    alt=""
                    src="https://static.overlay-tech.com/assets/7138f456-4b59-4bc1-b724-89a221603bcb.svg"
                  />
                </div>
                <div className={styles.flexWrapperTwelve}>
                  <p className={styles.caNhanHoaTimKiem}>
                    Dam bao co ve
                  </p>
                  <p className={styles.noiDung1}>
                    Cam ket mang den cho ban hanh trinh tron
                    ven. Hoan ngay 150% neu ko co ve.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className={styles.footer}>
        <div className={styles.group18}>
          <p
            className={
              styles.congTyTnhhThuongMaiDichVuVeXeDat
            }
          >
            <strong
              className={
                styles.congTyTnhhThuongMaiDichVuVeXeDatEmphasis0
              }
            >
              Cong ty TNHH Thuong mai dich vu VeXeDat
              <br />
              <br />
            </strong>
            Địa chỉ đăng ký kinh doanh: 8C Chữ Đồng Tử,
            Phường 7, Quận Tân Bình, Thành Phố Hồ Chí Minh,
            Việt Nam
            <br />
            Địa chỉ: Lầu 8,9, Tòa nhà CirCO, 222 Điện Biên
            Phủ, Quận 3, TP. Hồ Chí Minh, Việt Nam
            <br />
            Giấy chứng nhận ĐKKD số 0315133726 do Sở KH và
            ĐT TP. Hồ Chí Minh cấp lần đầu ngày 27/6/2018
            <br />
            Bản quyền © 2021 thuộc về VeXeDat.ml
          </p>
        </div>
        <div className={styles.flexWrapperTwo}>
          <div className={styles.group9}>
            <div className={styles.flexWrapperTen}>
              <div className={styles.relativeWrapperTwo}>
                <div className={styles.tuyenduongTwo}>
                  <p className={styles.tuyenDuong}>
                    Tuyen duong
                  </p>
                  <p className={styles.tuyenduong}>
                    <br />
                    Xe di Buon Ma Thuot giuong nam
                    <br />
                    <br />
                    Xe di Vung Tau tu Sai Gon
                    <br />
                    <br />
                    Xe di Nha Trang tu Sai Gon
                    <br />
                    <br />
                    Xe di Da Lat tu Sai Gon
                    <br />
                    <br />
                    Xe di SaPa tu Ha Noi
                    <br />
                    <br />
                    Xe di Hai Phong tu Ha Noi
                    <br />
                    <br />
                    Xe di Vinh tu Ha Noi
                  </p>
                </div>
                <div className={styles.tintuc}>
                  <p className={styles.tinTuc}>Tin tuc</p>
                  <p className={styles.xeGiuongNam}>
                    <br />
                    Xe giuong nam Limouse - dinh cao moi cua
                    nganh xe khach
                    <br />
                    <br />
                    Xe Limouse di Vung Tau: Tong hop top 6
                    xe khach chat luong cao
                    <br />
                    <br />
                    Review xe Limouse di Da Lat: nhung cau
                    hoi thuong gap
                    <br />
                    <br />
                    Xe Limouse di Sa Pa: Tong hop top cac
                    tuyen xe chat luogn cao
                  </p>
                </div>
              </div>
              <div className={styles.group17}>
                <p className={styles.xeLimousine}>
                  Xe limousine
                </p>
                <p className={styles.limousine}>
                  <br />
                  Xe Limousine di Buon Ma Thuot tu <br />
                  <br />
                  Xe Limousine di Vung Tau tu Sai Gon
                  <br />
                  <br />
                  Xe Limousine di Nha Trang
                  <br />
                  <br />
                  Xe Limousine Da Lat tu Sai Gon
                  <br />
                  <br />
                  Xe Limousine di SaPa tu Ha Noi
                  <br />
                  <br />
                  Xe Limousine di Hai Phong tu Ha Noi
                  <br />
                  <br />
                  Xe Limousine di Vinh tu Ha Noi
                </p>
              </div>
            </div>
            <div className={styles.flexWrapperTen}>
              <div className={styles.group17}>
                <p className={styles.benXe}>Ben xe</p>
                <p className={styles.benxe}>
                  Ben xe mien Dong
                  <br />
                  <br />
                  Ben xe Nuoc Ngam
                  <br />
                  <br />
                  Ben xe Trung tam Da Nang
                  <br />
                  <br />
                  Ben xe An Suong
                  <br />
                  <br />
                  Ben xe My Dinh
                  <br />
                  <br />
                  Ben xe Mien Tay
                </p>
              </div>
              <div className={styles.flexWrapperOne}>
                <p
                  className={
                    styles.conMotDongNhaXeNuaMaLuoiVietQua
                  }
                >
                  Con mot dong nha xe nua ma luoi viet qua
                </p>
                <div className={styles.tintuc}>
                  <p className={styles.nhaXe}>Nha xe</p>
                  <p className={styles.benxe}>
                    Xe Sao Viet
                    <br />
                    <br />
                    Xe taxi Hoa Mai
                    <br />
                    <br />
                    Xe Ha Long Travel
                    <br />
                    <br />
                    Xe Quoc Dat
                    <br />
                    <br />
                    Xe Thanh Binh xanh
                    <br />
                    <br />
                    Xe Thien Thanh Limousine
                    <br />
                    <br />
                    Xe Hong Son Phu Yen
                    <br />
                    <br />
                    Xe Tien Oanh
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className={styles.group12}>
            <div className={styles.relativeWrapperOne}>
              <p className={styles.hoTro}>Ho tro</p>
              <div className={styles.tintuc}>
                <p className={styles.veChungToi}>
                  Ve chung toi
                </p>
                <div className={styles.flexWrapperTen}>
                  <p
                    className={
                      styles.phanMemNhaXePhanMemDaiLyGioiThi
                    }
                  >
                    Phan mem nha xe
                    <br />
                    <br />
                    Phan mem dai ly
                    <br />
                    <br />
                    Gioi thieu VeXeDat.ml
                    <br />
                    <br />
                    Tuyen dung
                    <br />
                    <br />
                    Tin tuc
                    <br />
                    <br />
                    Lien he
                  </p>
                  <p
                    className={
                      styles.huongDanThanhToanQuyCheVeXeDatMl
                    }
                  >
                    Huong dan thanh toan
                    <br />
                    <br />
                    Quy che VeXeDat.ml
                    <br />
                    <br />
                    Chinh sach bao mat thong tin
                    <br />
                    <br />
                    Chinh sach bao mat thanh toan
                    <br />
                    <br />
                    Chinh sach khieu nai
                    <br />
                    <br />
                    Cau hoi thuong gap
                    <br />
                    <br />
                    Phan mem hang xe
                    <br />
                    <br />
                    Tra cuu don hang
                  </p>
                </div>
              </div>
            </div>
            <div className={styles.group17}>
              <p className={styles.veChungToi}>
                Chung nhan
              </p>
              <img
                alt=""
                className={styles.rectangle43}
                src="https://static.overlay-tech.com/assets/b2096724-3379-41ed-b90b-1063be84bbd3.png"
              />
              <img
                alt=""
                className={styles.rectangle43}
                src="https://static.overlay-tech.com/assets/eee295ab-87b3-4c69-8c65-f8be93cb1920.png"
              />
            </div>
            <div className={styles.group17}>
              <p className={styles.veChungToi}>
                Tai ung dung VeXeDat
              </p>
              <img
                alt=""
                src="https://static.overlay-tech.com/assets/9aec642c-13a3-4298-87c0-24d3f4200c60.png"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FirstPage;